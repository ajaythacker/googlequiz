import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import 'bootswatch/dist/pulse/bootstrap.min.css'; // Added this :boom:

ReactDOM.render(<App />, document.getElementById('root'));

