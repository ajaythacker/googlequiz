//1046586897889-ls1ho40b995rs51d9rcbcqcgt93ho522.apps.googleusercontent.com
import React from 'react';
import axios from 'axios';


class GoogleAuth extends React.Component{
    state = {
        isSignedIn:null
       
    }
    onSignIn = ()=>{
        console.log('clicked')
        this.auth.signIn().then(res=>{
            const userEmail =this.auth.currentUser.get().getBasicProfile().getEmail();
            const userName = this.auth.currentUser.get().getBasicProfile().getName();
            console.log(userEmail);
            this.props.saveUser(userEmail,userName);
            // axios.post('/api/createUser',{userEmail}).then(response=>{
            //     console.log('response',response)
            //     this.setState({userName,userEmail})
            // })

        });
    }

    onSignOut = ()=>{
        this.auth.signOut();
    }

    renderAuthButton = ()=>{
        if (this.state.isSignedIn){
            return  <p className="bs-component" onClick={this.onSignOut}>
                 <button  type="button" className="btn btn-danger">SignOut {this.props.userName}</button>
                 </p>
        }else{
            return  <p className="bs-component" onClick={this.onSignIn}>
                <button  type="button" className="btn btn btn-warning">Sign In with Google</button>
            </p> 
        }
    }

    //calls this callback function whenever user signs in / sign out even from 
    //some other browser tab
    onAuthChange =()=>{
        this.setState({isSignedIn:this.auth.isSignedIn.get()});
    }

   

    componentDidMount(){
        window.gapi.load('client:auth2',()=>{
            window.gapi.client.init({
                clientId:'1046586897889-ls1ho40b995rs51d9rcbcqcgt93ho522.apps.googleusercontent.com',
                scope:'email'
            }).then(()=>{
                this.auth = window.gapi.auth2.getAuthInstance();
                this.setState({isSignedIn:this.auth.isSignedIn.get()});

                //if the user signs out, and we want to know about it,
                //we can subscribe to the listen function and pass our 
                //callback function so that when user signs out / in, it will
                //invoke our callback function and we can check status
                this.auth.isSignedIn.listen(this.onAuthChange)
            })
        })
    }
    render(){
        return(
            <div className="nav-link">
                {this.renderAuthButton()}
            </div>
        )
    }
}

export default GoogleAuth;