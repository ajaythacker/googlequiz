import React from 'react';

class Quiz extends React.Component{

    render(){
        return(
            <div  class="container" style={{marginTop:"100px"}}>
               
                <div class="page-header" id="banner">
                    <div class="row">
                        <div class="col-lg-8 col-md-7 col-sm-6">
                            <h1>Quiz</h1>
                            <p class="lead">Quiz content appears here</p>
                        </div>
                        <div class="col-lg-4 col-md-5 col-sm-6">
                            <div class="sponsor">
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bs-docs-section clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 id="type-blockquotes">Here are your questions</h2>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="bs-component">
                            <form>
                        <fieldset>
                        <fieldset class="form-group">
                            <legend>Question 1</legend>
                            <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1"  />
                                <label for="optionsRadios1">Option one is this and that&mdash;be sure to include why it's great</label>
                                
                            </label>
                            </div>
                            <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2" />
                                <label for="optionsRadios2">Option two can be something else and selecting it will deselect option one
                               </label>
                               
                            </label>
                            </div>
                            <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios3" value="option3"  />
                                <label for="optionsRadios3">Option three </label>
                               
                            </label>
                            </div>
                        </fieldset>
                        </fieldset>
                        <fieldset class="form-group">
                        <p className="bs-component" onClick={this.props.saveQuiz}>
                 <button  type="button" className="btn btn-primary">Submit</button>
                 </p>
                        </fieldset>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}

export default Quiz;