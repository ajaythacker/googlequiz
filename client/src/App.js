import React from 'react';
import { BrowserRouter, Route,Link, Switch } from 'react-router-dom';
import Module from './components/Module';
import Quiz from './components/Quiz';
import Header from './components/Header';
import axios from 'axios';
import { throws } from 'assert';

class App extends React.Component {
  state = {
    quizSaved:null,
    isSignedIn:null,
    userName:null,
    userEmail:null
  }
  saveUser = (userEmail,userName)=>{
    axios.post('/api/createUser',{userEmail}).then(response=>{
      console.log('response',response)
      this.setState({userName,userEmail})
  })
  }
  saveQuiz = ()=>{
    const data = {userEmail:this.state.userEmail,quizScore:5}
    axios.post('/api/saveQuiz',data).then(response=>{
      console.log('response',response)
      this.setState({quizSaved:true})
  })
  }

  render(){
    
    return (
      <BrowserRouter>
         <Header userName={this.state.userName} userEmail={this.state.userEmail} saveUser={this.saveUser}/> 
         <Switch>
            <Route path="/module" component={Module} />
            <Route path="/quiz" 
               render={(props) => <Quiz {...props} {...this.state} saveQuiz={this.saveQuiz} />}/>
           
        </Switch>
    </BrowserRouter>
    );
  }

}

export default App;
