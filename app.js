const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/routes');
const mongoose = require('mongoose');

const app = express();

if (process.env.NODE_ENV === 'production') {
    //First check to see if there is a specific file which Express can match in client build
    //To make Express serve up production assets like main.js or main.css file
    app.use(express.static('client/build'));

    //If someone makes a request for a route that express does not understand,
    //then make Express serve up the index.html file
    const path = require('path');
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

//this will create new db called googlequiz if it doesn't exist, and connect to it.
if (process.env.NODE_ENV !== 'production') {
mongoose.connect('mongodb://localhost:27017/googlequiz',{useNewUrlParser:true,useUnifiedTopology:true}).then(res=>{
    console.log('mongodb connected')
}).catch(err=>console.log('mongo failed',err))
}else{
mongoose.connect('mongodb://ajay:Abcdefgh1@ds041561.mlab.com:41561/googlequiz',{useNewUrlParser:true,useUnifiedTopology:true}).then(res=>{
    console.log('mongodb connected')
}).catch(err=>console.log('mongo failed',err))
}

app.use(bodyParser.json()); //app use bodyparser should be above the routes call

routes(app);

module.exports = app;