const QuizController = require('../controller/quiz_controller')

module.exports = (app)=>{
    app.post('/api/createUser',QuizController.createUser);
    app.post('/api/saveQuiz',QuizController.saveQuiz)
      app.get('/api/user',QuizController.getUsers);

     app.get('/api/user/:id',QuizController.getUser);

    //  app.post('/api/user',QuizController.saveUser);

         
}