const Quiz = require('../models/quiz')


module.exports = {

    //this method gets invoked from the routes file
    greeting(req,res){
        res.send({hi : 'there'})
    },

    getUsers(req,res){
        Quiz.find().then(users=>res.send(users))
    },

    getUser(req,res){
        console.log('getuser params',req.params)
        const userId = req.params.id;
        Quiz.findById(userId).then(user=>{
            console.log('user',user)
            res.send(userId)
        })
    },

    createUser(req,res){
        console.log('createuser invoked')
        const userProps = req.body;
        console.log('req body',req.body);
        //check if user already exists 
        Quiz.findOne({userEmail: userProps.userEmail}).then(user=>{
            if (user){
                console.log('user already exists',user)
                res.send(user)
            }else{
                Quiz.create(userProps).then(user=> {
                    console.log('user',user)
                    res.send(user)}
                    );
            }
            
        });
        
       
    },
    saveQuiz(req,res){
        console.log('saveQuiz invoked')
        const userProps = req.body;
        console.log('req body',req.body);
        //check if user already exists 
       
        Quiz.updateOne({userEmail:userProps.userEmail},userProps).then(user=> {
            console.log('user',user);
            res.send(user)
        });
            
            
        }
    }

   


